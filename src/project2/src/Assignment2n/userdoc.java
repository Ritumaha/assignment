package Assignment2n;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap; 
import java.util.List; 
import java.util.Map;
import java.util.Objects; 
import java.util.stream.Collectors;

public class userdoc
{
	public static void main(String args[]) throws IOException
	{

		 List<Person> people = new ArrayList<>();
		 people.add(new Person("X",19,"tata", "cuttack"));
		 people.add(new Person("y",20,"wipro", "bbsr"));
		 people.add(new Person("z",18,"infosys", "jspur"));
		 people.add(new Person("a",19,"TechMahendra", "cuttack"));
		 people.add(new Person("ln",23,"Tcs", "bangaluru"));
		 people.add(new Person("rb",22,"wpro", "bbsr"));
		 people.add(new Person("sh",24,"Systools", "Assam"));
		 people.add(new Person("tj",26,"Tcs", "Electronic city"));
		 people.add(new Person("ll",25,"accenture", "Noida"));
		 people.add(new Person("d",30,"Cerebronics", "bhubaneswar"));
		 
		 Map<Integer,List<Person>> personByAge = people.stream() .collect(Collectors.groupingBy(Person::getAge)); 
		 System.out.println("Person grouped by age in Java 8: " + personByAge);

	}
}

class Person

	{ 
	
	private String name;
	private String city;
	private int age; 
	private String company;
	public Person(String name,int age,String company,String city) 
	{ 
	this.name = name;
	this.age = age; 
	this.company = company;
	this.city = city;
	}
	public String getName()
	{
        return name;
	}
	public void setName(String name)
	{ 
		this.name = name;
	}
	
	public String getCity()
		{
		return city;
		}
	public void setCity(String city) 
		{
		this.city = city;
		}

	public int getAge() 
		{ 
		return age; 
		}
	public void setAge(int age) 
		{ 
		this.age = age;
		}
	 
	public int hashCode()
		{
		int hash = 7;
		hash = 79 * hash + Objects.hashCode(this.name);
		hash = 79 * hash + this.age;
		hash = 79 * hash + Objects.hashCode(this.company);
		hash = 79 * hash + Objects.hashCode(this.city);
		
		return hash;
		}
	 
	    public boolean equals(Object obj) 
	 	{
		 if (obj == null)
		 {
			 return false; 
		 }
		 
		 if (getClass() != obj.getClass()) 
		 	{
			 return false; 
			 }
		
		 final Person other = (Person) obj;
		 if (!Objects.equals(this.name, other.name)) 
		 	{
			 return false; 
			 }

		 if (!Objects.equals(this.city, other.city))
		 	{
	            return false;
	        }
		 
		 if (!Objects.equals(this.company, other.company))
		 	{
	            return false;
	        }
		 
		 if (this.age != other.age) 
		 	{
			 return false;
		 	}
		 return true;
	 	}

	 
		 
	}
		 
	 
	


package Assignment2n;
import java.util.*;
import java.util.Collections;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
public class prnp
{

	public static void main(String[] args) throws Exception
	{
		List<Integer> numbers=Arrays.asList(1,2,3,4,5,6,7,8,9,10);
		System.out.println("primes numbers are:");
		System.out.println(numbers.stream().filter(prnp::isPrime).collect(Collectors.toList()));
		System.out.println("nonprime numbers are:");
		System.out.println(numbers.parallelStream().filter(prnp::nonPrime).collect(Collectors.toList()));
	}
		private static boolean isPrime(int number)
		{
		for(int i=2;i<=number /2;i++)
		{
			if(number % i==0)
			{
				return false;
				
			}
			
			}
		return true;
          
	}
		public static boolean nonPrime(int number)
		{
			for(int i=2;i<=number/2;i++)
			{
				if(number % i==0)
				{
					return true;
				}
			}
			return false;
		}
}

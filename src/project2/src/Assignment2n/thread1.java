package Assignment2n;
public class thread1 {
	public static void main(String[] args) {
 
		OddEvenRunnable oddRunnable=new OddEvenRunnable(1);
		OddEvenRunnable evenRunnable=new OddEvenRunnable(0);
 
		Thread t1=new Thread(oddRunnable,"Odd");
		Thread t2=new Thread(evenRunnable,"Even");
		
		t1.start();
		t2.start();
 
	}
}

class OddEvenRunnable implements Runnable{
	 
	public int n=20;
	static int  num=1;
	int remainder;
	static Object lock=new Object();
 
	OddEvenRunnable(int remainder)
	{
		this.remainder=remainder;
	}
 
	
	public void run() 
	{
		while (num < n)
		{
			synchronized (lock) 
			{
				while (num % 2 != remainder) 
				{ 
				
					try {
						lock.wait();
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
				}
				System.out.println(Thread.currentThread().getName() + " " + num);
				num++;
				lock.notifyAll();
			}
		}
	}
}
 

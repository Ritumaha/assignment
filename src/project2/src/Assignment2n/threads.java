package Assignment2n;
public class threads 
{
	public static void main(String[] args) 
	{
 
		OddEvenRunnables oddRunnable=new OddEvenRunnables(1);
		OddEvenRunnables evenRunnable=new OddEvenRunnables(0);
 
		Thread t1=new Thread(oddRunnable,"Odd");
		Thread t2=new Thread(evenRunnable,"Even");
		
		t1.start();
		t2.start();
		try 
		{
			Thread.sleep(1000);
			}
		catch(Exception e)
		{
			
		}
		System.out.println("FINISHED EXECUTION");
 
	}
}

class OddEvenRunnables  implements Runnable
{
	 
	public int n=25;
	static int  num=1;
	int remainder;
	static Object lock=new Object();
 
	OddEvenRunnables(int remainder)
	{
		this.remainder=remainder;
	}
 
	
	public void run() 
	{
		while (num < n)
		{
			synchronized (lock) 
			{
				while (num % 2 != remainder) 
				{ 
				
					try {
						lock.wait();
					} 
					catch (InterruptedException e) 
					{
						e.printStackTrace();
					}
				}
				System.out.println(Thread.currentThread().getName() + " " + num);
				num++;
				lock.notifyAll();
			}
		}
	}
}

